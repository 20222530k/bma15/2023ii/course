/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jguillenl.classes.example;

/**
 *
 * @author PROFESOR
 */
public class Tv {

    int channel;
    int volumeLevel;
    boolean on;
    public boolean isOn(){
        return on;
    }

    //limites
    final int minVolumeLevel = 1;
    final int maxVolumeLevel = 7;
    final int minChannel = 1;
    final int maxChannel = 120;

    public Tv() {
        this.channel = 1;
        this.volumeLevel = 1;
        this.on = false;
        

    }

    public void turnOn() {
        this.on = true;
    }

    public void turnOff() {
        this.on = false;
    }

    public void setChannel(int newChannel) {
        this.channel = newChannel;
    }

    public void setVolumen(int newVolumeLevel) {
        this.volumeLevel = newVolumeLevel;
    }

    public void channelUp() {
        if (on && channel < maxChannel) {
            channel++;
        } else if (channel == maxChannel) {
            channel = minChannel;
        }
    }

    public void channelDown() {
        if (on && channel > maxChannel) {
            channel--;
        } else if (channel == minChannel) {
            channel = maxChannel;
        }
    }

    public void volumeUp() {
        if (on && volumeLevel < maxVolumeLevel) {
            volumeLevel++;
        }

    }

    public void volumeDown() {
        if (on && volumeLevel > minVolumeLevel) {
            volumeLevel--;
        }
    }

    //te genera el estado de tus variables 
    @Override
    public String toString() {
        return "Tv{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }

}

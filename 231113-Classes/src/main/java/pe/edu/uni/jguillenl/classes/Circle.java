/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jguillenl.classes;

/**
 *
 * @author PROFESOR
 */
public class Circle {
    double radious;
    Circle(){
        this.radious = 1;
        
    }
    Circle (double newRadious){
    this.radious = newRadious;
    }
    double getArea(){
        return Math.PI*radious*radious;
    }
    
    double getPerimetro(){
        return Math.PI*2*radious;
    }
    void setRadious (double newRadious){
    this.radious = newRadious;
    }
    //se sobreescirbe los datos de la clase padre en la clase hija 

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }
    
    
}

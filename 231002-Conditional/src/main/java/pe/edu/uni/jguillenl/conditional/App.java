/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jguillenl.conditional;

/**
 *
 * @author Justin Guillen <justin.guillen.l@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        /*int i = 35;
        if (30 < i) {
            System.out.println("el número es mayor a 30");
        }
        if (i % 2 == 1) {
            System.out.println("Es impar");
        }
        if (i % 2 == 0) {
            System.out.println("    eS PAR");
        }
        System.out.println("------------------");
        int j = 31;
        if (30 < j) {
            System.out.println("el numero es mayor a 30");
        } else {
            if (j % 2 == 1) {
                System.out.println("Es impar");
            } else {
                if (j % 2 == 0) {
                    System.out.println("ES par");
                }
            }

        }
        System.out.println("------------------");
        if (30 < i) {
            System.out.println("el número es mayor a 30");
            return;
        }
        if (i % 2 == 1) {
            System.out.println("Es impar");
            return;
        }
        if (i % 2 == 0) {
            System.out.println("    eS PAR");
            return;
        }
        */
        int m = 11;
        int i = 0;
        switch (i%2){
            case 0:
                System.out.println("Es par");
                break;
            case 1:
                System.out.println("Es impar");
                break;
            default:
                System.out.println("Cualquier otro numero");
               
        }
    }
}

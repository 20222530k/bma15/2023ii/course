/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jguillenl.interfaces;

/**
 *
 * @author PROFESOR
 */
public class TestInterfaces {

    public static void main(String[] args) {
        System.out.println("Interface");
        Object[] objects = {new Orange(), new Apple(), new Chicken(), new Tiger()};
        for (Object object : objects) {
            if (object instanceof Animal) {
                System.out.println(((Animal) object).sound());

            }
            if (object instanceof Edible) {
                System.out.println(((Edible) object).howToEat());
            }

        }
    }
}

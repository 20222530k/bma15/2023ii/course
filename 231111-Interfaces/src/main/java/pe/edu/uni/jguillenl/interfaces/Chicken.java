/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jguillenl.interfaces;

/**
 *
 * @author PROFESOR
 */
public class Chicken extends Animal implements Edible{

    @Override
    public String sound() {
        return "pio pipipipi";
    }

    @Override
    public String howToEat() {
        return " Chicken : fry it !!";
    }
    
}

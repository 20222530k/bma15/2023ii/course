package pe.edu.uni.jguillenl.boceto;

public class Boceto {

    private String usuario;
    private int saldo;
    private int clave;
    

    public Boceto(String usuario, int saldo, int clave) {
        this.usuario = usuario;
        this.saldo = saldo;
        this.clave = clave;
    }

    public String getUsuaio() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }
   
}

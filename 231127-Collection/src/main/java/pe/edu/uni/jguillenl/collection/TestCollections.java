/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jguillenl.collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author PROFESOR
 */
public class TestCollections {

    public static void main(String[] args) {
        System.out.println("Test Collection");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Junin", "Iquitos"};
        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add(cities[0]);
        collection1.add(cities[1]);
        collection1.add(cities[2]);
        collection1.add(cities[3]);

        System.out.println("Una lista de ciudades : " + collection1);
        System.out.println("La ciudad " + cities[3] + "esta presente en la lista: " + collection1.contains(cities[3]));
        if (collection1.contains(cities[5])) {

            System.out.println("La ciudad " + cities[3] + " está contenida!");

        } else {
            System.out.println("La ciudad " + cities[4] + " no está contenida!");
        }
        if (collection1.remove(cities[4])) {

            System.out.println(cities[4] + "ha sido removido");
        } else {
            System.out.println("no ha bsido removida");
        }
        int i = 3;
        if (collection1.remove(cities[i])) {

            System.out.println(cities[i] + "ha sido removido");
        } else {
            System.out.println(cities[i] + "no ha bsido removida");
        }

        System.out.println(collection1.size() + "ciudades presents");
        
        Collection<String> collection2  = new ArrayList<>();
        collection2.add(cities[4]);
        collection2.add(cities[5]);
        collection2.add(cities[5]);
        collection2.add(cities[1]);
        
        System.out.println("Lista de ciudades en collection2: " + collection2);
        ArrayList<String> c1;
        c1 = (ArrayList<String>) collection1.clone();
        c1.addAll(collection2);
        System.out.println("Ciudades en collection1 o collection2: " + c1);
        c1 = (ArrayList<String>) collection1.clone();
        c1.retainAll(collection2);
        System.out.println("Ciudades en collection1 y collection2: "+ c1);
        
    }
    
}

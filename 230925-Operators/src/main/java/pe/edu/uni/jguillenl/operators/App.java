/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jguillenl.operators;

import java.io.File;

/**
 *
 * @author Justin Guillen <justin.guillen.l@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.print("-");
        System.out.println("Hello World!");
        System.out.print("+");
        
        int y1=4;
        double x1 = 3+2* --y1;
        System.out.println("x1: " + x1);x1--;
        System.out.println(+ x1);
        
        //arithmetic operators
        int x2 = 2 * 5 + 3 * 4 - 8;
        System.out.println("x2: " + x2);
        
        int x3 = 2 * ((5 + 3) * 4 - 8);
        System.out.println("x2: " + x3);
        
        System.out.println(9/3);
        System.out.println(9%3);
        System.out.println(10/3);
        System.out.println(10%3);
        
        int x4 = 1;
        long y4 = 33;
        System.out.println(x4 * y4);
        System.out.println(Integer.SIZE/8);
        System.out.println(Long.SIZE/8);
        System.out.println("    ");    
        
        boolean x8 = false;
        // Booleam x9 = false;
        System.out.println("x8" + x8);
        x8=!x8;
        System.out.println("x8" + x8);
        
        int x = 3;
        int y = ++x * 5 / x-- + -- x;
        System.out.println("x: " + x);
        System.out.println("y: " + y);
        
        int a1 = (int)1.8;
        System.out.println("a1 ="+ a1);
        short b1 = (short)1921222;
        System.out.println("b1 =" + b1 );
        int c1 = (int)9f;
        System.out.println("c1 =" + c1 );
        long d1 = 192301398193818323L;
        System.out.println("d1 =" + d1 );
        
        short a2 = 10;
        short b2 = 3;
        short c2 = (short)(a2*b2);
        System.out.println("c2 = " + c2);
        int a3 = 2, b3 = 3;
        //a3 = a3 * b3;//asignación simple
        a3 *= b3;// asignación compuesta
        System.out.println("a3 = "+ a3);
        long a4 = 18;
        int b4 = 5;
        // b4 = b4 * a4;
        b4 *= a4;
        
        long a5 = 5;
        long b5 = (a5 = 3);
        System.out.println("a5 " + a5);
        System.out.println("b5 " + b5);
        
        int a6 = 10, b6 = 20, c6 = 30;
        System.out.println(a6 < b6);
        System.out.println(a6 <= b6);
        System.out.println(a6 >= b6);
        System.out.println(a6 > c6);
        
        boolean a7  = true || (y < 4);
        System.out.println("a7 =" + a7);
        
        Object o;
        o = new Object ();
        if (o != null && o.hashCode() < 5){
           //do something
        }
        if (o != null & o.hashCode() < 5){
           //do something
        }
        
        int a8 = 6;
        boolean b8 = (a8 >= 6) || (++a8 <= 7);
        System.out.println("a8 "+ a8);
        
        boolean b9 = false ;
        boolean a9 = (b9 = true );
        System.out.println("a9"+ a9);
        
        System.out.println("Equality operator");
        File p1 = new File("file.txt");
        File q1 = new File("file.txt");
        File r1 = p1;
        System.out.println("p1 ==q1?" + (p1==q1));
        System.out.println("p1 ==r1?" + (p1==r1));
        
        
       
        
        
        
    }
}
